﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;

namespace Assets.Resources.Code.States
 {
	class WonSateLevel_1:IStateBase
	{
		private StateManager manager;   // manager game class type
		

		public WonSateLevel_1(StateManager managerRef)    // constructor
		{
			manager = managerRef;   // store a reference to StateManager class
			
			// Load Scene02
			if (Application.loadedLevelName != "Scene02")
			{
				Application.LoadLevel("Scene02");
			}
		}
		

		public void StateUpdate()
		{
		}
		

		public void ShowIt()
		{
			GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
			myButtonStyle.fontSize = 50;
			
			myButtonStyle.normal.textColor = Color.green;
			myButtonStyle.hover.textColor = Color.blue;
			GUI.backgroundColor = Color.white;
			// draw the texture store in instructionStateSplash variable that is the size of the screen
			GUI.DrawTexture(new Rect(50 , 20 , 1200, 650), manager.gameDataRef.beginStateBackground, ScaleMode.StretchToFill);
			
			GUI.Box(new Rect(Screen.width/2-300, 50, 400, 100), "Won state",myButtonStyle);
			
			// if button pressed or any keys
			if (GUI.Button(new Rect(Screen.width/2-350, Screen.height/2-Screen.height / 4,800,100), "Press or S Key to Level two",myButtonStyle) || Input.GetKeyUp(KeyCode.S))
			{
				// switch the state to Setup state
				manager.SwitchState(new PlayStateLevel_2(manager));
			}

			if (GUI.Button (new Rect (Screen.width / 2-350, Screen.height / 2, 800,100), "Press Here or K Key to start again",myButtonStyle) || Input.GetKeyUp (KeyCode.K))

			
			{
				// switch the state to Setup state
				manager.SwitchState(new SetupState(manager));
			}


			//Debug.Log("In WonState");
		}
  }
}