﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Resources.Code.Interfaces;

namespace Assets.Resources.Code.States 
{
public class BeginState:IStateBase
	{
	
		private StateManager manager;
		public BeginState(StateManager manageRef)
		{
			manager = manageRef;
			if (Application.loadedLevelName != "Scene00") {
				Application.LoadLevel("Scene00");
			}
		}

		public void StateUpdate()
		{

		}


		public void ShowIt()
		{

			GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
			myButtonStyle.fontSize = 60;
			GUI.color = Color.white;

			myButtonStyle.normal.textColor = Color.white;
			myButtonStyle.hover.textColor = Color.red;
			// draw the texture store in beginStateSplash variable that is the size of the screen
			GUI.DrawTexture(new Rect(50 , 20 , 1200, 650),manager.gameDataRef.beginStateBackground, ScaleMode.StretchToFill);
			
			GUI.Box(new Rect(Screen.width/2-300, 150, 600, 100), "Begin State",myButtonStyle);

			//Debug.Log("In BeginState");
			GUI.Label(new Rect(Screen.width/2-300 ,75,600, 100),"Cheese Hunt Game",myButtonStyle);
//			if (GUI.Button(new Rect(Screen.width/2-300, Screen.height/2,600,100),"Play Game", myButtonStyle))
//			{

//				
//			}
			if (GUI.Button(new Rect(Screen.width/2-350, Screen.height/2,800,100), "Press or S Key to Continue",myButtonStyle) || Input.GetKeyUp(KeyCode.S) )
			{
				
				manager.SwitchState(new SetupState(manager));
			}
			if (GUI.Button(new Rect(Screen.width/2-200, Screen.height/2 +200,400,100),"Options",myButtonStyle))
			{

				
			}


		}

	}
}
