﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;

namespace Assets.Resources.Code.States
  {
class InstructionState:IStateBase
	{
		private StateManager manager;   // manager game class type
		
	
		public InstructionState(StateManager managerRef)    // constructor
		{
			manager = managerRef;   // store a reference to StateManager class
			
			// Load Scene03
			if (Application.loadedLevelName != "Scene03")
			{
				Application.LoadLevel("Scene03");
			}
		}
		
	
		public void StateUpdate()
		{
		}
		

		public void ShowIt()
		{

			GUI.DrawTexture(new Rect(Screen.width/2 - 256 / 2, Screen.height/2 - 128 /2, 256, 128), manager.gameDataRef.beginStateBackground, ScaleMode.StretchToFill);
			
			GUI.Box(new Rect(Screen.width / 2 - 160 / 2, Screen.height / 2, 160, 120), "Instruction state");
			
			// if button pressed or R keys
			if (GUI.Button(new Rect(10, 10, 250, 60), "Press Here or Any R to Return") || Input.GetKeyUp(KeyCode.R) )
			{
				// switch the state to Setup state
				manager.SwitchState(new SetupState(manager));
			}
			Debug.Log("In InstructionState");
		}


  }
}