﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;

namespace Assets.Resources.Code.States
{
	class LostStateLevel_3:IStateBase
	{
		private StateManager manager;   // manager game class type
		

		public LostStateLevel_3(StateManager managerRef)    // constructor
		{
			manager = managerRef;   // store a reference to StateManager class
			
			// Load Scene02
			if (Application.loadedLevelName != "Scene06")
			{
				Application.LoadLevel("Scene06");
			}
		}
		

		public void StateUpdate()
		{
		}
		

		public void ShowIt()
		{
			GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
			myButtonStyle.fontSize = 50;
			
			myButtonStyle.normal.textColor = Color.green;
			myButtonStyle.hover.textColor = Color.white;
			GUI.backgroundColor = Color.white;


			GUI.DrawTexture(new Rect(50 , 20 , 1200, 650), manager.gameDataRef.beginStateBackground, ScaleMode.StretchToFill);

			GUI.Box(new Rect(Screen.width/2-200, 50, 400, 100), "Lost state",myButtonStyle);
			
			// if button pressed or any keys
			if (GUI.Button(new Rect(Screen.width/2-350, Screen.height/2-Screen.height / 4,800,100), "Press Here or S Key to return",myButtonStyle) || Input.GetKeyUp(KeyCode.S))
			{
				// switch the state to Setup state
				manager.SwitchState(new SetupState(manager));
			}

			if (GUI.Button(new Rect(Screen.width/2-350, Screen.height/2- Screen.height/6,800,100), "Press Here or P Key to play Again",myButtonStyle) || Input.GetKeyUp(KeyCode.A))
			{
				// switch the state to Setup state
				manager.SwitchState(new PlayStateLevel_3(manager));
			}
			//Debug.Log("In LostState");
		}
}
}