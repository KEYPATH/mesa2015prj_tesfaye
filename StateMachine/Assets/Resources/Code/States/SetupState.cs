﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;

namespace Assets.Resources.Code.States {
class SetupState:IStateBase
	{

		private StateManager manager;   

			public SetupState(StateManager managerRef)    // constructor
		{
			manager = managerRef;   
			
		
			if (Application.loadedLevelName != "Scene00")
			{
				Application.LoadLevel("Scene00");
			}
		}
		

		public void StateUpdate()
		{
		}
		

		public void ShowIt()
		{
			GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
			myButtonStyle.fontSize = 60;
			
			myButtonStyle.normal.textColor = Color.blue;
			myButtonStyle.hover.textColor = Color.gray;


			GUI.DrawTexture (new Rect (50 , 20 , 1200, 650), manager.gameDataRef.beginStateBackground, ScaleMode.StretchToFill);
			
			GUI.Box(new Rect(Screen.width/2-300, 100, 600, 100), "Setup state",myButtonStyle);
			
		
			if (GUI.Button(new Rect(Screen.width/2-350, Screen.height/2 +100,800,100), "Press or I Key to Instruction",myButtonStyle) || Input.GetKeyUp(KeyCode.I))
			{
			
				manager.SwitchState(new InstructionState(manager));
			}
			myButtonStyle.normal.textColor = Color.green;
			if(GUI.Button(new Rect(Screen.width/2-350, Screen.height/2-100,800,100), "Press Here or P to Play",myButtonStyle) || Input.GetKeyUp(KeyCode.P))
				//if (GUI.Button(new Rect(Screen.width/2-350, Screen.height/2-100,800,100), manager.gameDataRef.beginStateBackground,myButtonStyle) || Input.GetKeyUp(KeyCode.P))
			{

				manager.gameDataRef.forceManager.RemainingTimeSetted = 5;
				
			
				manager.gameDataRef.forceManager.ForceLowerLimit = 5;
				

				manager.SwitchState(new PlayStateLevel_1(manager));
			}
			
			//Debug.Log("In SetupState");
		}
 }
}