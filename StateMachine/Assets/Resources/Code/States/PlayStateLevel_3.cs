﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;
using Assets.Resources.Code.Scripts;

namespace Assets.Resources.Code.States
{
	class PlayStateLevel_3:IStateBase
	{
		bool levelOver = false;
		private StateManager manager;   // manager game class type
		private string levelName_3 = "Hard";

		public PlayStateLevel_3(StateManager managerRef)    // constructor
		{
			manager = managerRef;   


			if (Application.loadedLevelName != "Scene05")
			{
				Application.LoadLevel("Scene05");
			}
			manager.gameDataRef.GetBoardManager ().CreateLayoutMap (levelName_3);
		}
		

		public void StateUpdate()
		{
		

            // if mouse is present update the force value
//            if (Input.mousePresent)
//            {
//
//                manager.gameDataRef.forceManager.UpdateForce(Input.GetKey(KeyCode.A), Input.GetKey(KeyCode.B));
//
//              
//            }
        } 
     
        public void ShowIt()
		{
			GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
			myButtonStyle.fontSize = 20;
			
			myButtonStyle.normal.textColor = Color.white;
			myButtonStyle.hover.textColor = Color.gray;
			GUI.backgroundColor = Color.white;
            if (GUI.Button(new Rect(10, 25, 250, 60), "Press Here or  W to Won",myButtonStyle) || Input.GetKeyUp(KeyCode.W))
            {
				levelOver = true;
				manager.gameDataRef.ResetLevel(levelOver);
				manager.SwitchState(new WonSateLevel_3(manager));
			}

            // if button pressed or L keys
            if (GUI.Button(new Rect(Screen.width/2 - 125, 25, 250, 60), "Press Here or L to Lose", myButtonStyle) || Input.GetKeyUp(KeyCode.L))
            {
				levelOver = true;
				manager.gameDataRef.ResetLevel(levelOver);
				manager.SwitchState(new LostStateLevel_3(manager));
            }

            // if button pressed or R keys
            if (GUI.Button(new Rect(Screen.width - 250 - 5, 25, 250, 60), "Press Here or R to Return", myButtonStyle) || Input.GetKeyUp(KeyCode.R))
            {
				levelOver = true;
				manager.gameDataRef.ResetLevel(levelOver);
                manager.SwitchState(new SetupState(manager));
            }


            Debug.Log("In PlayState");
        }
 }
}