﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic; 
[ExecuteInEditMode]

public class Grid : MonoBehaviour {


	public enum gridType{XbyY,clickToRemove}
	public gridType gType;
	public enum OrientatiionType{topLeft}
	public OrientatiionType orientation;

	public int Xcoords ;
	public int Ycoords;
	public int Xspacing;

	public int Yspacing;
	public Vector3 startingPosition;
	public string layoutName = "";

	public bool register = false;
	public bool createGrid = false;
	public bool clearGrid = false;
	public bool save = false;

	public GameObject SlotPrefab;
	public GameObject GridRoot;

	public List<GameObject> slots = new List<GameObject>();
	public List<GameObject> slotsCopy = new List<GameObject>();

	//GameObject deleteFloor;
	private Vector3 mousePos;
	private Vector3 worldPos;
	private List <Vector2> gridPosition =  new List <Vector2>();



	// Use this for initialization
	void Start () {
		#if UNITY_EDITOR
		Debug.Log("Unity Editor");
		#endif
		
		#if UNITY_STANDALONE_WIN
		Debug.Log("Stand Alone Windows");
		#endif

	}
	
	// Update is called once per frame
	void Update () {

		CreateGrid ();
		ClearGrid ();
		Register ();
		Save ();

	

		}
	


	void ClearGrid()
	{
		
		if (clearGrid) {
			clearGrid= false;
			for(int i = 0;i<slots.Count;i++)
			{
				DestroyImmediate(slots[i]);
				//DestroyImmediate(GameObject.FindWithTag("Background"));
				
			}
			//DestroyImmediate(deleteFloor);

			slots.Clear();
			slotsCopy.Clear();
			gridPosition.Clear();
			
		}	
	}

	void Save()
	{

		if (save) {
		
			save = false;
			PrefabUtility.CreatePrefab("Assets/Resources/Prefabs/"+layoutName+".prefab", GridRoot);
		
		}
	


	}
	void CreateGrid()
	{
		if (createGrid) {
			int index = 0;
			createGrid= false;

				for(int x= 0; x< Xcoords;x++){
					for(int y= 0; y< Ycoords;y++)
					{

					GameObject go = Instantiate(SlotPrefab,Vector3.zero, Quaternion.identity) as  GameObject;
					switch(orientation)
					{
					case OrientatiionType.topLeft:
						

						go.transform.position = new Vector3(startingPosition.x + x* Xspacing,
						                                    startingPosition.y + y* Yspacing,
						                                    startingPosition.z);
				    
					  
						go.name = "Slot"+ index.ToString();
					     
					
						gridPosition.Add (new Vector2(x,y));
				break;
			}

					go.transform.parent = GridRoot.transform;

				
					index++;

					slots.Add (go);
				
					slotsCopy.Add (go);

				}

			}


		
		}
	}

	void Register()
	{

    if (register) {
		
			register = false;

			SceneView.onSceneGUIDelegate -= OnClickToRemove; 
			SceneView.onSceneGUIDelegate += OnClickToRemove; 
		}

	}
	void OnClickToRemove(SceneView sceneview)
	{
		Debug.LogWarning ("qui");
		Event e = Event.current;
		if (e.isMouse && e.button == 0 && e.type == EventType.MouseDown) {

				Ray r = Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x,
		                                           -e.mousePosition.y +Camera.current.pixelHeight));

		RaycastHit hit;
//
		if (Physics.Raycast(r, out hit))
			
			{

				slots.Remove(hit.collider.gameObject);

				DestroyImmediate(hit.collider.gameObject);


				}
				
				
	
			}
//			e.Use();

	}

}




