﻿using UnityEngine;
using System.Collections;

public class Slot : MonoBehaviour {

	Layout laySlots;
	[HideInInspector]
	public bool walkable;
	[HideInInspector]
	public int gCost;
	[HideInInspector]
	public int hCost;
	[HideInInspector]
	public Vector2  ID;
	[HideInInspector]
	public int gridSizeX, gridSizeY;
	[HideInInspector]
	public int gridX, gridY; 
	[HideInInspector]
	public Vector3 positionOfNode;

	public Slot parent;

	void OnDrawGizmos()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawWireCube (transform.position, new Vector3 (1, 1, 2));

		Gizmos.color = Color.white;
		Gizmos.DrawCube (transform.position, new Vector3 (1, 1, 0));


	}
	
	public Slot(bool _walkable,Vector2 _ID, int _gridX, int _gridY, Vector3 _positionOfNode)
	{
		this.walkable = _walkable;
		this.ID = _ID;
		this.gridX = _gridX;
		this.gridY = _gridY;
		this.positionOfNode = _positionOfNode;
    foreach (Slot slt in laySlots.slots) {
		
			if(this.positionOfNode == slt.transform.localPosition)
			{
				this.walkable = false;

			}
		}

		
	}
	public int fCost { 
		
		get {
			
			return gCost + hCost;
			
		}
	}
}
