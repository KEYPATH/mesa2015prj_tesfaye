﻿using UnityEngine;
using System;
using UnityEditor;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

[System.Serializable]
public class BoardManage : MonoBehaviour
{
	public Layout layout;
	private static BoardManage boardMap;
	public GameObject board;
	public GameObject wallPrefab;
	public GameObject floorPrefab;
	public GameObject enemyPrefab;
	public GameObject exitPrefab;
	public GameObject playerPrefab;
	public GameObject foodPrefab;
	public GameObject pathBlockPrefab;
	public GameObject keyPrefab;
	public string layoutMap;
	public bool gameWon;
	List<int> indexList = new List<int> ();
	List<Slot> itemSlot = new List<Slot> ();
	public List<Slot> slotCopy = new List<Slot> ();
	public List<Slot> slotDeleted = new List<Slot> ();
	private int level = 32;
	private int mediumLevel = 128;
	private int hardLevel = 256;
	private int very = 256;




	private string easy = "Easy";
	private string medium = "Medium";
	private string hard = "Hard";
	private string hard2 = "VeryHard";
	private string type  ;
	int column = 12;
	int row = 19;
	public class Count
	{
		public int minimum;             //Minimum value for our Count class.
		public int maximum;             //Maximum value for our Count class.
		
		
		//Assignment constructor.
		public Count (int min, int max)
		{
			minimum = min;
			maximum = max;
		}
	}
	
	//Number of rows in our game board.
	public Count foodCount = new Count (2, 4);                      //Lower and upper limit for our random number of walls per level.
	public Count keyCount = new Count (1, 1);
	public Count playerCount = new Count (1, 1);
	public Count exitCount = new Count (1, 1);
	
	void Awake ()
	{
		//Call the InitGame function to initialize the first level 

		InitGame ();
	}

	//Initializes the game for each level.
	void InitGame ()
	{


		boardMap = this;
		//DontDestroyOnLoad (gameObject);
		//GameManager.Instance ().SetDifficulty (GameManager.GameDifficulty.Easy);

		//  type = GameManager.Instance ().ReturnLevel ();

	}

//	public void SetUpScene (int level) {
//		
//		//CreateLayoutMap ();
//		CreateWallsLayoutMap ();
//		CreateFloorLayoutMap ();
//		LayoutObjectAtRandom (foodPrefab, foodCount.minimum, foodCount.maximum);
//		playerPrefab.transform.localScale = new Vector3 (3.0f,  3.0f, 1);
//		LayoutObjectAtRandom (playerPrefab, playerCount.minimum, playerCount.maximum);
//		LayoutObjectAtRandom (keyPrefab, keyCount.minimum, keyCount.maximum);
//		LayoutObjectAtRandom (exitPrefab, exitCount.minimum, exitCount.maximum);
//		
//		int enemyCount = (int)Mathf.Log(level, 2f);
//		enemyPrefab.transform.localScale = new Vector3(0.8f,0.8f,1);
//		
//		//Instantiate a random number of enemies based on minimum and maximum, at randomized positions.
//		LayoutObjectAtRandom (enemyPrefab, enemyCount, enemyCount);
//
//
//	}
	public static BoardManage GetBoardManager ()
	{
		return boardMap;
	}

	// Update is called once per frame
	
	public void CreateLayoutMap (string s)
	{  

		slotDeleted.Clear ();		
		slotCopy.Clear ();
		itemSlot.Clear ();
		layout.slots.Clear ();


		int enemyCount = 0;
		int pathBlockCount = 0;
		string food = "food";
		string player = "player";
		string key = "key";
		string exit = "exit";
		string block = "block";
		string enemy = "enemy";
 
		if (s == "Easy") {
			level = level;
			board = layout.GetRandomLayout (s);
			enemyCount = (int)Mathf.Log (level, 2f);
			pathBlockCount = (int)Mathf.Log (level, 2f);
		}

		if (s == "Medium") {
			board = layout.GetRandomLayout (s);
			level = mediumLevel;
			enemyCount = (int)Mathf.Log (level, 2f);
			pathBlockCount = (int)Mathf.Log (level, 2f);
		}

		if (s == "Hard") {
			board = layout.GetRandomLayout (s);
			level = hardLevel;
			enemyCount = (int)Mathf.Log (level, 2f);
			pathBlockCount = (int)Mathf.Log (level, 2f);
		}

		if (s == "VeryHard") {
			board = layout.GetRandomLayout (s);
			level = very;
			enemyCount = (int)Mathf.Log (level, 2f);
			pathBlockCount = (int)Mathf.Log (level, 2f);
		}


		board.transform.parent = gameObject.transform;

		CreateWallsLayoutMap ();
		CreateFloorLayoutMap ();

	
		LayoutObjectAtRandom (foodPrefab, foodCount.minimum, foodCount.maximum, food);
		playerPrefab.transform.localScale = new Vector3 (3.0f, 3.0f, 1);
		LayoutObjectAtRandom (playerPrefab, playerCount.minimum, playerCount.maximum, player);
		LayoutObjectAtRandom (keyPrefab, keyCount.minimum, keyCount.maximum, key);
		LayoutObjectAtRandom (exitPrefab, exitCount.minimum, exitCount.maximum, exit);


		pathBlockPrefab.transform.localScale = new Vector3 (0.05f, 0.075f, 1);
		enemyPrefab.transform.localScale = new Vector3 (0.4f, 0.4f, 1);

	
		LayoutObjectAtRandom (enemyPrefab, enemyCount, enemyCount, enemy);


		LayoutObjectAtRandom (pathBlockPrefab, pathBlockCount, pathBlockCount, block);
	}

	void CreateFloorLayoutMap ()
	{

		GameObject map = Resources.Load ("Prefabs/" + layoutMap) as GameObject;
		
		GameObject original = GameObject.Instantiate (map, Vector3.zero, Quaternion.identity) as GameObject;
		original.transform.parent = transform;
	
		foreach (Slot slt in original.GetComponentsInChildren<Slot>()) {

			slotCopy.Add (slt);

			string slotname = slt.name;
			int index = 0;
			bool isItTheSame = false;
			foreach (Slot s in layout.slots) {
				
				string slotnameCopy = s.name;
			
				isItTheSame = slotname.Equals (slotnameCopy);
				
				if (isItTheSame) {
					index++;
					//	slotDeleted.Add(slotCopy[ii]);
				}
				
			}
			if (index == 0) {
				
				slotDeleted.Add (slt);
			}
		}


		foreach (Slot s in slotCopy) {
  

			GameObject go = GameObject.Instantiate (floorPrefab, s.transform.position, Quaternion.identity) as GameObject;
			go.transform.localScale = new Vector3 (1.4f, 1.4f, 1);
			Destroy (s.GetComponent<BoxCollider> ());
			go.transform.parent = s.transform;

		}
		for (int j = 13; j< slotCopy.Count-column; j++) {

			//	if (j > column && j % column != 0 && j - column - 1 % column != 0 && j < slotCopy.Count-1- column) {
			if (j % column != 0 && j % column != 11) {

				indexList.Add (j);
			

			}
			
		}

	}
	
	void CreateWallsLayoutMap ()
	{
		foreach (Slot s in layout.slots) {
			GameObject go = GameObject.Instantiate (wallPrefab, s.transform.position, Quaternion.identity) as GameObject;
			go.transform.localScale = new Vector3 (1.0f, 1.0f, 1);
			Destroy (s.GetComponent<BoxCollider> ());
			go.transform.parent = s.transform;
			
		}
	}

	public void LayoutObjectAtRandom (GameObject tilePrefab, int min, int max, string namePrefab)
	{
		int randomIndex = 0;
		Slot ss;
		// break  - exit looping
		//continue -exit current loop
	
	
			
		int objectCount = Random.Range (min, max);
  
		for (int i = 0; i<objectCount; i++) {
     	
			if (namePrefab == "block") {
                
				PathDirectionFinder (tilePrefab);
			} else {
		
				randomIndex = RandomSlot ();
				ss = slotDeleted [randomIndex];

				itemSlot.Add (ss);
				slotDeleted.RemoveAt (randomIndex);

				GameObject go = GameObject.Instantiate (tilePrefab, ss.transform.localPosition, Quaternion.identity) as GameObject;
				go.transform.parent = ss.transform;
                
			}

		}


	}

	void PathDirectionFinder (GameObject tilePrefab)
	{
		float angleDegreeX = 1.0f;
		float angleDegreeY = 1.0f;
		float rotationHorizontal = 0.0f;
		float rotationVertical = 0.0f;

		bool checkWallPosition = false;
		bool checkItemPosition = false;

		//Debug.Log ("index list" + indexList.Count);
		for (int t = 0; t< indexList.Count; t++) {

			angleDegreeX = 90.0f;
			angleDegreeY = 180.0f;


			checkWallPosition = false;
			checkItemPosition = false;
			int tt = Random.Range (0, indexList.Count);

			Slot finderSlot = slotCopy [indexList [tt]];
			string nameOfSlot = finderSlot.name;
			//Vector3 posSlot = finderSlot.transform.position;
			
			foreach (Slot si in layout.slots) {
				//	if (posSlot == si.transform.position) 
				if (nameOfSlot.Equals (si.name)) {
					checkWallPosition = true;
					break;
				}

			}
			
			foreach (Slot slotItem in itemSlot) {
				//if (posSlot == slotItem.transform.position) 
				if (nameOfSlot.Equals (slotItem.name)) {
					checkItemPosition = true;
					break;
				}
			
			}
			if (checkWallPosition == true && checkItemPosition == false || checkItemPosition == true && checkWallPosition == false) {

				continue;
			}

			if (checkWallPosition == false && checkItemPosition == false) {

            	
				string compareNameX_ = slotCopy [indexList [tt] - column].name;
				string compareNameX = slotCopy [indexList [tt] + column].name;
				string compareNameY_ = slotCopy [indexList [tt] - 1].name;
				string compareNameY = slotCopy [indexList [tt] + 1].name;

				int checkItemExistence = IsItemNoughberhood (compareNameX_, compareNameX, compareNameY_, compareNameY);

				if (checkItemExistence == 1 || checkItemExistence == -1) {

					int checkWallExistence = IsWallNoughberhood (compareNameX_, compareNameX, compareNameY_, compareNameY);
					if (checkItemExistence == 1 && checkWallExistence == 1) {
						GameObject go = GameObject.Instantiate (tilePrefab, finderSlot.transform.localPosition, Quaternion.identity) as GameObject;
						
						go.transform.rotation = Quaternion.Euler (transform.rotation.x, transform.rotation.y, transform.rotation.z + angleDegreeX + rotationHorizontal);
						
						go.transform.parent = finderSlot.transform;
						
						//rotationHorizontal += 180;
						indexList.RemoveAt (tt);
						
						break;

					}
					if (checkItemExistence == -1 && checkWallExistence == -1) {
						GameObject go = GameObject.Instantiate (tilePrefab, finderSlot.transform.localPosition, Quaternion.identity) as GameObject;
						
						go.transform.rotation = Quaternion.Euler (transform.rotation.x, transform.rotation.y, transform.rotation.z + angleDegreeY + rotationVertical);
						
						go.transform.parent = finderSlot.transform;
						
						//rotationVertical += 180;
						indexList.RemoveAt (tt);

						break;

					}
				} else {
					continue;
				}

			} 

		}

	}

	int IsItemNoughberhood (string compareNameX_, string compareNameX, string compareNameY_, string compareNameY)
	{
		int x_ = 0, x = 0, y_ = 0, y = 0;
		int t = 1;

		foreach (Slot sxI_ in itemSlot) {

			if (compareNameX_.Equals (sxI_.name)) {
				x_++;
				break;
			}

			if (compareNameX.Equals (sxI_.name)) {

				x++;
				break;
			}
		}
		foreach (Slot syI_ in itemSlot) {
			if (compareNameY_.Equals (syI_.name)) {
				y_++;
				break;
			}

			if (compareNameY.Equals (syI_.name)) {
				y++;
				break;
			}
		}

		if (x_ == 0 && x == 0) 
		
			return t;
		if (y_ == 0 && y == 0)
			return -t;
		else 
			return 0;

	}

	int IsWallNoughberhood (string compareNameX_, string compareNameX, string compareNameY_, string compareNameY)
	{
		int x_ = 0, x = 0, y_ = 0, y = 0;
		int t = 1;
		foreach (Slot sx_ in layout.slots) {
			
		
			if (compareNameX_.Equals (sx_.name)) {
				x_++;
				break;
			}
	
			if (compareNameX.Equals (sx_.name)) {
			
				x++;
				break;
				
			}
		}
	
		// neighbourhood in vertical direction 
		 
		foreach (Slot sy_ in layout.slots) {
		 
			if (compareNameY_.Equals (sy_.name)) { 
				y_++;
				break;
			}
		
			if (compareNameY.Equals (sy_.name)) {
				y++;
				break;
			}
		} 
		if (x_ == 0 && x == 0)
			return t;
		if (y_ == 0 && y == 0)
			return -t;
		else
			return 0; 
	}

	int RandomSlot ()
	{
		//Declare an integer randomIndex, set it's value to a random number between 0 and the count of items in our List gridPositions.
		int randomIndex = Random.Range (0, slotDeleted.Count);
		//Debug.Log ("random index" +randomIndex);
//		Slot randomSlot = slotDeleted [randomIndex];
//
//		slotDeleted.RemoveAt (randomIndex);
//		return randomSlot;
		return randomIndex;
	}

	void GameOver ()
	{
		gameWon = true;
	}

	void update ()
	{

	}

	
}

//		Debug.Log ("Original slot" + slotCopy.Count);
//		Debug.Log ("Path slot" + layout.slots.Count);
//		Debug.Log ("Number of element: " + slotDeleted.Count);
//		int ii, jj;
//		for (ii = 0; ii< slotCopy.Count; ii++) {
//			string slotname = slotCopy[ii].name;
//		 	int index = 0;
//			bool isItTheSame = false;
//			for ( jj = 0; jj< layout.slots.Count; jj++){
//			
//				string slotnameCopy = slotCopy[jj].name;
//   
//
//			    isItTheSame = slotname.Equals(slotnameCopy);
//
//				if (isItTheSame) 
//				{
//					index++;
//				//	slotDeleted.Add(slotCopy[ii]);
//				}
//
//			}
//			if (index==0)
//			{
//
//				slotDeleted.Add(slotCopy[ii]);
//			}

//	}
//	Debug.Log ("Number of element: " + slotDeleted.Count);

//		print("add" + slotDeleted.Count);
//		slotDeleted = slotDeleted.Distinct ().ToList ();
//		Debug.Log ("Number of element: " + slotDeleted.Count);

//		foreach (Slot slt in slotCopy.ToArray) {
//			string slotname = slt.name;
//			foreach (Slot s in layout.slots.ToArray) {
//				string slotnameCopy = s.name;
//				if (slotname.e) {
//					slotDeleted.Add (slt);
//				}
//			
//			}
//
//		}

//slotDeleted = slotCopy.Except(layout.slots).ToList();
//slotDeleted = slotCopy.Union (layout.slots);


//		
//		foreach (Slot s in layout.slots) {
//			string slotnameCopy = s.name;
//			print("dd"+slotnameCopy);
//			
//			bool isItTheSame = false;
//			
//			isItTheSame = slotname.Equals(slotnameCopy);
//			if (isItTheSame == false) {
//				slotDeleted.Add(slt);
//				}
//				
//			}
//		}
//  

//		foreach (Slot ss in slotDeleted) {
//			string slotd = ss.name;
//
//			print ("deleted" +slotd);
//	
//		}


//						horizontalAllign = true;
//						print("horizontal");
//						
//						
//						//slotDeleted.ElementAtOrDefault(randomIndex);
//
//					if ((slotDeleted[randomIndex-2] != null)&&(slotDeleted[randomIndex-1] != null)&&
//					    (slotDeleted[randomIndex+1] != null)&&(slotDeleted[randomIndex+2] != null))
//					{	
//						verticalAllign = true;
//						print("vertical");
//						




//				if (horizontalAllign || verticalAllign)
//
//				  break;


//				if(verticalAllign)
//				{
//									ss = slotDeleted[randomIndex];
//					     			slotDeleted.RemoveAt(randomIndex);
//					GameObject go = GameObject.Instantiate (tilePrefab, ss.transform.localPosition, Quaternion.identity) as GameObject;
//					
//					go.transform.rotation =Quaternion.Euler(transform.rotation.x ,transform.rotation.y,transform.rotation.z + rotationVertical);
//					go.transform.parent = ss.transform;
//					rotationVertical += 180.0f;
//				}
//			   if(horizontalAllign)
//				{
//					ss = slotDeleted[randomIndex];
//					slotDeleted.RemoveAt(randomIndex);
//
//            	GameObject go = GameObject.Instantiate (tilePrefab, ss.transform.localPosition, Quaternion.identity) as GameObject;
//				
//					go.transform.rotation =Quaternion.Euler(transform.rotation.x ,transform.rotation.y,transform.rotation.z + rotationHorizontal);
//				go.transform.parent = ss.transform;
//				rotationHorizontal += 180.0f;
//				}