﻿using UnityEngine;
using System;
using UnityEditor;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Random = UnityEngine.Random;

using Assets.Resources.Code.Interfaces;

namespace Assets.Resources.Code.Scripts
{
	[System.Serializable]
	public class GameData: MonoBehaviour,IGameData
	{
		public Slot slotPlayer;
		public Slot slotExit;
		public List<Slot> path;
		public GameObject board;
		public ForceManager forceManager;
		GameObject original;
		public Layout layout;
		Grid grid;
		private static GameData boardMap;
		public GameObject wallPrefab;
		public GameObject floorPrefab;
		public GameObject enemyPrefab;
		public GameObject exitPrefab;
		public GameObject playerPrefab;
		public GameObject foodPrefab;
		public GameObject pathBlockPrefab;
		public GameObject keyPrefab;
		public GameObject fullMap;
		public GameObject mapManager;
		public Texture2D beginStateBackground;
		public Texture2D setupStateBackground;
		public List <string> levels = new List<string> ();
		private bool gameWon; 
		List<int> indexList = new List<int> ();
		List<Slot> itemSlot = new List<Slot> ();
		public List<Slot> slotCopy = new List<Slot> ();
		public List<Slot> slotDeleted = new List<Slot> ();
		private int level = 32;
		private int mediumLevel = 128;
		private int hardLevel = 256;
		private int very = 256;
		private string easy = "Easy";  
		private string medium = "Medium"; 
		private string hard = "Hard"; 
		private string hard2 = "VeryHard"; 
		private string type  ;
		int column = 12;// y direction number of slot
		int row = 19; // xdirection number of slot 
		public Vector3 startPosition;
		public  Vector3 targetPosition;
		public class Count
		{
			public int minimum;             //Minimum value for our Count class.
			public int maximum;             //Maximum value for our Count class.
			
			
			//Assignment constructor.
			public Count (int min, int max)
			{
				minimum = min;
				maximum = max;
			}
		}
		
		//Number of rows in our game board.
		public Count foodCount = new Count (2, 4);                      //Lower and upper limit for our random number of walls per level.
		public Count keyCount = new Count (1, 1);
		public Count playerCount = new Count (1, 1);
		public Count exitCount = new Count (1, 1);

		Slot[,] Nodes;

		void CreateNodesFromMap()
		{

					
			Nodes = new Slot[grid.Xcoords, grid.Ycoords];
			for (int x= 0; x< grid.Xcoords; x++) {
				for (int y= 0; y< grid.Ycoords; y++) {

					Vector3 nodePosition = new Vector3(grid.startingPosition.x +x*grid.Xspacing ,grid.startingPosition.y +y*grid.Yspacing,grid.startingPosition.z);
					Nodes[x,y] = new Slot(true,new Vector2(x,y), x,y, nodePosition );
					 
				}
			}

		}
		void Awake ()
		{
		
			
			InitGame ();
		}

		void InitGame ()
		{
			
			boardMap = this;

		}
		
		public GameData GetBoardManager ()
		{
			return boardMap;
		}
		
		public void CreateLayoutMap (string s)
		{  
			
			slotDeleted.Clear ();		
			slotCopy.Clear ();
			itemSlot.Clear ();
			layout.slots.Clear ();
			
			
			int enemyCount = 0;
			int pathBlockCount = 0;
			string food = "food";
			string player = "player";
			string key = "key";
			string exit = "exit";
			string block = "block";
			string enemy = "enemy";

			if (s == "Easy") {
				level = level; 
				board = layout.GetRandomLayout (s);
				enemyCount = (int)Mathf.Log (level, 2f);
				pathBlockCount = (int)Mathf.Log (level, 2f);
			}

			if (s == "Medium") {
				board = layout.GetRandomLayout (s);
				level = mediumLevel;
				enemyCount = (int)Mathf.Log (level, 2f);
				pathBlockCount = (int)Mathf.Log (level, 2f);
			}
			
			if (s == "Hard") {
				board = layout.GetRandomLayout (s);
				level = hardLevel;
				enemyCount = (int)Mathf.Log (level, 2f);
				pathBlockCount = (int)Mathf.Log (level, 2f);
			}

			if (s == "VeryHard") {
				board = layout.GetRandomLayout (s);
				level = very;
				enemyCount = (int)Mathf.Log (level, 2f);
				pathBlockCount = (int)Mathf.Log (level, 2f);
			}
			
			
			board.transform.parent = gameObject.transform;
			
			CreateWallsLayoutMap ();
			CreateFloorLayoutMap ();
			
			
			LayoutObjectAtRandom (foodPrefab, foodCount.minimum, foodCount.maximum, food);
			playerPrefab.transform.localScale = new Vector3 (3.0f, 3.0f, 1);


			//<summary>
			// place the player at the beginning
			// place the exit at the end 
			//<summary>

			LayoutObjectAtRandom (playerPrefab, playerCount.minimum, playerCount.maximum, player);
			LayoutObjectAtRandom (keyPrefab, keyCount.minimum, keyCount.maximum, key);
			LayoutObjectAtRandom (exitPrefab, exitCount.minimum, exitCount.maximum, exit);
			
			
			pathBlockPrefab.transform.localScale = new Vector3 (0.05f, 0.075f, 1);
			enemyPrefab.transform.localScale = new Vector3 (0.4f, 0.4f, 1);
			
			
			LayoutObjectAtRandom (enemyPrefab, enemyCount, enemyCount, enemy);
			
			
			LayoutObjectAtRandom (pathBlockPrefab, pathBlockCount, pathBlockCount, block);

			///<c>
			/// this is where the node generation goes to find "OPTIMAL PATH"
			/// </c>
		//	CreateNodesFromMap ();
		}


		void CreateFloorLayoutMap ()
		{

			//GameObject map = Resources.Load ("Prefabs/" + layoutMap) as GameObject;
			
			original = GameObject.Instantiate (fullMap, Vector3.zero, Quaternion.identity) as GameObject;
			original.transform.parent = transform;
			
			foreach (Slot slt in original.GetComponentsInChildren<Slot>()) {
				
				slotCopy.Add (slt);
				
				string slotname = slt.name;
				int index = 0;
				bool isItTheSame = false;
				foreach (Slot s in layout.slots) {
					
					string slotnameCopy = s.name;
					
					isItTheSame = slotname.Equals (slotnameCopy);
					
					if (isItTheSame) {
						index++;
						//	slotDeleted.Add(slotCopy[ii]);
					}
					
				}
				if (index == 0) {
					
					slotDeleted.Add (slt);
				}
			}
			
			
			foreach (Slot s in slotCopy) {
				
				
				GameObject go = GameObject.Instantiate (floorPrefab, s.transform.position, Quaternion.identity) as GameObject;
				go.transform.localScale = new Vector3 (1.4f, 1.4f, 1);
				Destroy (s.GetComponent<BoxCollider> ());
				go.transform.parent = s.transform;
				
			}
			for (int j = column+1; j< slotCopy.Count-column; j++) {
				
				//	if (j > column && j % column != 0 && j - column - 1 % column != 0 && j < slotCopy.Count-1- column) {
				if (j % column != 0 && j % column != column-1) {
					
					indexList.Add (j);
					
					
				}
				
			}
			
		}

		void CreateWallsLayoutMap ()
		{
			foreach (Slot s in layout.slots) {
				GameObject go = GameObject.Instantiate (wallPrefab, s.transform.position, Quaternion.identity) as GameObject;
				go.transform.localScale = new Vector3 (1.0f, 1.0f, 1);
				Destroy (s.GetComponent<BoxCollider> ());
				go.transform.parent = s.transform;
				
			}
		}
		
		public void LayoutObjectAtRandom (GameObject tilePrefab, int min, int max, string namePrefab)
		{
			int randomIndex = 0;
			Slot ss;
			// break  - exit looping
			//continue -exit current loop
			
			
			
			int objectCount = Random.Range (min, max);
			
			for (int i = 0; i<objectCount; i++) {
				
				if (namePrefab == "block") {
					
					PathDirectionFinder (tilePrefab);
				}

				else if (namePrefab =="player")
				{

		            slotPlayer = slotDeleted[0];
					startPosition = slotPlayer.transform.localPosition;
					slotDeleted.RemoveAt(0);
					GameObject go = GameObject.Instantiate (tilePrefab, slotPlayer.transform.localPosition, Quaternion.identity) as GameObject;
					go.transform.parent = slotPlayer.transform;
				}
				else if (namePrefab =="exit")
				{
					
					 slotExit = slotDeleted[slotDeleted.Count -1];
					targetPosition = slotExit.transform.localPosition;
					slotDeleted.RemoveAt(slotDeleted.Count -1);
					GameObject go = GameObject.Instantiate (tilePrefab, slotExit.transform.localPosition, Quaternion.identity) as GameObject;
					go.transform.parent = slotExit.transform;
				}

				else {
					
					randomIndex = RandomSlot ();
					ss = slotDeleted [randomIndex];
					
					itemSlot.Add (ss);
					slotDeleted.RemoveAt (randomIndex);
					
					GameObject go = GameObject.Instantiate (tilePrefab, ss.transform.localPosition, Quaternion.identity) as GameObject;
					go.transform.parent = ss.transform;
					
				}
				
			}
			
			
		}
		
		void PathDirectionFinder (GameObject tilePrefab)
		{
			float angleDegreeX = 1.0f;
			float angleDegreeY = 1.0f;
			float rotationHorizontal = 0.0f;
			float rotationVertical = 0.0f;
			
			bool checkWallPosition = false;
			bool checkItemPosition = false;
			
			//Debug.Log ("index list" + indexList.Count);
			for (int t = 0; t< indexList.Count; t++) {
				
				angleDegreeX = 90.0f;
				angleDegreeY = 180.0f;
				
				
				checkWallPosition = false;
				checkItemPosition = false;
				int tt = Random.Range (0, indexList.Count);
				
				Slot finderSlot = slotCopy [indexList [tt]];
				string nameOfSlot = finderSlot.name;
				//Vector3 posSlot = finderSlot.transform.position;
				
				foreach (Slot si in layout.slots) {
					//	if (posSlot == si.transform.position) 
					if (nameOfSlot.Equals (si.name)) {
						checkWallPosition = true;
						break;
					}
					
				}
				
				foreach (Slot slotItem in itemSlot) {
					//if (posSlot == slotItem.transform.position) 
					if (nameOfSlot.Equals (slotItem.name)) {
						checkItemPosition = true;
						break;
					}
					
				}
				if (checkWallPosition == true && checkItemPosition == false || checkItemPosition == true && checkWallPosition == false) {
					
					continue;
				}
				
				if (checkWallPosition == false && checkItemPosition == false) {
					
					
					string compareNameX_ = slotCopy [indexList [tt] - column].name;
					string compareNameX = slotCopy [indexList [tt] + column].name;
					string compareNameY_ = slotCopy [indexList [tt] - 1].name;
					string compareNameY = slotCopy [indexList [tt] + 1].name;
					
					int checkItemExistence = IsItemNoughberhood (compareNameX_, compareNameX, compareNameY_, compareNameY);
					
					if (checkItemExistence == 1 || checkItemExistence == -1) {
						
						int checkWallExistence = IsWallNoughberhood (compareNameX_, compareNameX, compareNameY_, compareNameY);
						if (checkItemExistence == 1 && checkWallExistence == 1) {
							GameObject go = GameObject.Instantiate (tilePrefab, finderSlot.transform.localPosition, Quaternion.identity) as GameObject;
							
							go.transform.rotation = Quaternion.Euler (transform.rotation.x, transform.rotation.y, transform.rotation.z + angleDegreeX + rotationHorizontal);
							
							go.transform.parent = finderSlot.transform;
							
							//rotationHorizontal += 180;
							indexList.RemoveAt (tt);
							
							break;
							
						}
						if (checkItemExistence == -1 && checkWallExistence == -1) {
							GameObject go = GameObject.Instantiate (tilePrefab, finderSlot.transform.localPosition, Quaternion.identity) as GameObject;
							
							go.transform.rotation = Quaternion.Euler (transform.rotation.x, transform.rotation.y, transform.rotation.z + angleDegreeY + rotationVertical);
							
							go.transform.parent = finderSlot.transform;
							
							//rotationVertical += 180;
							indexList.RemoveAt (tt);
							
							break;
							
						}
					} else {
						continue;
					}
					
				} 
				
			}
			
		}
		
		int IsItemNoughberhood (string compareNameX_, string compareNameX, string compareNameY_, string compareNameY)
		{
			int x_ = 0, x = 0, y_ = 0, y = 0;
			int t = 1;
			
			foreach (Slot sxI_ in itemSlot) {
				
				if (compareNameX_.Equals (sxI_.name)) {
					x_++;
					break;
				}
				
				if (compareNameX.Equals (sxI_.name)) {
					
					x++;
					break;
				}
			}
			foreach (Slot syI_ in itemSlot) {
				if (compareNameY_.Equals (syI_.name)) {
					y_++;
					break;
				}
				
				if (compareNameY.Equals (syI_.name)) {
					y++;
					break;
				}
			}
			
			if (x_ == 0 && x == 0) 
				
				return t;
			if (y_ == 0 && y == 0)
				return -t;
			else 
				return 0;
			
		}
		
		int IsWallNoughberhood (string compareNameX_, string compareNameX, string compareNameY_, string compareNameY)
		{
			int x_ = 0, x = 0, y_ = 0, y = 0;
			int t = 1;
			foreach (Slot sx_ in layout.slots) {
				
				
				if (compareNameX_.Equals (sx_.name)) {
					x_++;
					break;
				}
				
				if (compareNameX.Equals (sx_.name)) {
					
					x++;
					break;
					
				}
			}
			
			// neighbourhood in vertical direction 
			
			foreach (Slot sy_ in layout.slots) {
				
				if (compareNameY_.Equals (sy_.name)) { 
					y_++;
					break;
				}
				
				if (compareNameY.Equals (sy_.name)) {
					y++;
					break;
				}
			} 
			if (x_ == 0 && x == 0)
				return t;
			if (y_ == 0 && y == 0)
				return -t;
			else
				return 0; 
		}
		
		int RandomSlot ()
		{
			//Declare an integer randomIndex, set it's value to a random number between 0 and the count of items in our List gridPositions.
			int randomIndex = Random.Range (0, slotDeleted.Count);
			//Debug.Log ("random index" +randomIndex);
			//		Slot randomSlot = slotDeleted [randomIndex];
			//
			//		slotDeleted.RemoveAt (randomIndex);
			//		return randomSlot;
			return randomIndex;
		}
		
		void GameOver ()
		{
			gameWon = true;
		}

		void Start ()
		{
			//TO DO: when scene start set default value of player lives
			forceManager = new ForceManager ();
		}

		void Update ()
		{
		}

		public void ResetGameData ()
		{
			//TO DO: when scene Restart set default value of player lives
			forceManager.ResetForce ();
		}

		public List<Slot> GetNeighbourhoodSlot (Slot slot)
		{
			// TO DO convert node to SLOT
			List<Slot> neighbours = new List<Slot> ();
			float xPos = slot.transform.localPosition.x;
			float yPos = slot.transform.localPosition.y;
			float zPos = slot.transform.localPosition.z;
		
			for (int x=-1; x<=1; x++) {
				for (int y =-1; y<=1; y++) {
					if (x == 0 && y == 0)
						continue;
				
					int checkX = slot.gridX + x;
					int checkY = slot.gridY + y;
				

					// TO DO : check the slot position of the passed in Node

					if (checkX >= 0 && checkX < grid.Xcoords&& checkY >= 0 && checkY < grid.Ycoords) {
   					neighbours.Add (Nodes[checkX,checkY]);
			    	}
				}
			}
			return neighbours;
		}



		public void ResetLevel (bool levelOver)
		{
			if (levelOver) {
				Destroy (board);
				Destroy (original);
			}
		}
		///<Summary>
		/// check the slot and the slot conversion 
		///</Summary>
		//		void OnDrawGizmos()
		//
		//		{
		//			if (slotCopy != null) {
		//			
		//			foreach(Slot s in slotCopy)
		//
		//				{
		//					if (path.Contains(s))
		//						Gizmos.color = Color.black;
		// 				}
		//			}
		//		}
	}

}
