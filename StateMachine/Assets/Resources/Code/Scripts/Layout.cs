﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]

public class Layout  {
	
	public List<string>layoutNames = new List<string> ();
	private int index = 0;
	public List<Slot> slots = new List<Slot>();
	public string LevelType;
	//public string ss= "this";

	
	public GameObject GetRandomLayout(string s)
	{
		
	//return GetLayoutFromIndex (Random.Range (0, layoutNames.Count));
		//return GetLayoutFromIndex (index, levelOfGame);
		return GetLayoutFromNames (s);
	}
	
	
	public GameObject GetLayoutFromNames(string s)
	{
		
		return GetLayoutFromIndex (layoutNames.IndexOf(s));
		//return GetLayoutFromIndex (index,ss);
	}
	
	
	public GameObject GetLayoutFromIndex(int index )
	{
	//	LevelType = levelType;

	
		GameObject resource = Resources.Load ("Prefabs/"+ layoutNames[index]) as GameObject;
		
		GameObject go = GameObject.Instantiate (resource, Vector3.zero, Quaternion.identity) as GameObject;
		
		
		foreach (Slot slot in go.GetComponentsInChildren<Slot>()) {
			
			slots.Add(slot);
		}

		
		return go;
		
	}
	
}
