﻿using UnityEngine;
using System;

using System.Collections;
using System.Collections.Generic;

public class Node
{

	public bool walkable;
	public Vector3 worldPosition;
	public int gCost;
	public int hCost;
	public int gridSizeX, gridSizeY;
	public int gridX, gridY;


	public Node parent;

	public Node (bool _walkable, Vector3 _worldposition, int _gridX, int _gridY)
	{
		walkable = _walkable;
		worldPosition = _worldposition;

		gridX = _gridX;
		gridY = _gridY; 

	}

	public int fCost { 
	
		get {

			return gCost + hCost;

		}
	}

}
