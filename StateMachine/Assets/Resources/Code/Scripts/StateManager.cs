﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.Scripts;
using Assets.Resources.Code.States;

public class StateManager : MonoBehaviour {

	// variable to store the active state
	private IStateBase activeState;
	
	/* prevent showing this variable in Inspector of Unity
     * In our case GameData store only data about game and so there is no
     * reason to allow it changeable in the inspector
     */
	[HideInInspector]
	public GameData gameDataRef;    // game data reference
	
	private static StateManager instanceRef;    // game manager class
	
	/// <summary>
	/// method recalled one time right after scene is created. after object create and before game plays
	///  </summary>
	/// <returns></returns>
	void Awake()
	{
		
		/* if StateManger already exist destroy immediatly this object, unless create it 
         * and don't destroy it during all game. We want only one game maner class and create
         * it when we start the game
         */
		if (instanceRef == null)
		{
			instanceRef = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			// when BeginningScene is reloaded we destroy the new GamaManager cause we want only the original one
			DestroyImmediate(gameObject);
		}
	}
	
	/// <summary>
	/// Use this for initialization
	/// </summary>
	/// <returns></returns>
	void Start()
	{
		// setting up the beginning state
		activeState = new BeginState(this);
		
		//take refrence to game data only on times during game
		gameDataRef = GetComponent<GameData>();
	}
	
	/// <summary>
	/// Update is called once per frame
	/// </summary>
	/// <returns></returns>
	void Update()
	{
		if (activeState != null)
		{
			activeState.StateUpdate();
		}
		
		Debug.Log("I'm in update function");
	}
	
	/// <summary>
	/// Show text button etc. Is called at least one per frame but could be more
	/// </summary>
	/// <returns></returns>
	void OnGUI()
	{
		// if one state is active draw active state GUI
		if (activeState != null)
		{
			activeState.ShowIt();
		}
	}
	
	/// <summary>
	/// Switch from one state to other
	/// </summary>
	/// <returns></returns>
	public void SwitchState(IStateBase newSate_tmp)
	{
		// switch to new state
		activeState = newSate_tmp;
	}
	
	/// <summary>
	/// Destroy and reload Scene00
	/// </summary>
	/// <returns></returns>
	public void Restart()
	{
		// destroy all object also GameManager because we want to restart from scratch ( from zero )
		Destroy(gameObject);
		// load scene 00
		Application.LoadLevel("Scene00");
	}
}
