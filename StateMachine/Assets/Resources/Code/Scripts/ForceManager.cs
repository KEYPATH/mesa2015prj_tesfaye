﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Resources.Code.Interfaces;

namespace Assets.Resources.Code.Scripts
{
	public class ForceManager
	{
		
		// max value of force out of it I have an error
		public static float FORCE_MAX = 1000;
		// force limit
		float forceLowerLimit = 0;
		float forceUpperLimit = 800;
		// force value
		[HideInInspector]
		public float actualForce = 0;
		float prevForce = 0;
		// remaining target time
		int remainingTimeSetted = 0;
		// remaining time in thershold value
		[HideInInspector]
		public float remainingTime = 0;
		// memorizing if timer is active
		bool isTimerActive = false;
		// memorizing if we are in threshold force
		bool inThreshold = false;
		
		// mean value
		double meanValue = 0;
		// total number of data;
		int Nvalue = 0;
		// standard deviation of force
		double stdDevForce = 0;
		// sum of quadratic value
		double value2 = 0;
		// standard deviation limit
		public double stdDevForcelimit = 5;
		// true uf stdDev less than a value
		bool inStdDevForveLimit = false;
		
		/// <summary>
		/// constructor
		/// </summary>
		/// <returns></returns>        
		public ForceManager()
		{
			forceLowerLimit = 0;
			forceUpperLimit = 800;
			actualForce = 0;
			prevForce = actualForce;
			meanValue = 0;
			Nvalue = 0;
			stdDevForce = 0;
			value2 = 0;
			inStdDevForveLimit = false;
			
			ResetTimer();
		}
		
		/// <summary>
		/// property to manage remaining time setted
		/// </summary>
		/// <returns></returns>
		public int RemainingTimeSetted
		{
			get
			{
				return remainingTimeSetted;
			}
			set
			{
				remainingTimeSetted = value;
				remainingTime = value;
			}
		}
		
		/// <summary>
		/// property to manage lower force limit
		/// </summary>
		/// <returns></returns>
		public float ForceLowerLimit
		{
			get
			{
				return forceLowerLimit;
			}
			set
			{
				forceLowerLimit = value;
			}
		}
		
		/// <summary>
		/// property to manage upper force limit
		/// </summary>
		/// <returns></returns>
		public float ForceUpperLimit
		{
			get
			{
				return forceUpperLimit;
			}
			set
			{
				forceUpperLimit = value;
			}
		}
		
		/// <summary>
		/// return if threshold force bool state, true = in thereshold
		/// </summary>
		/// <returns></returns>
		public bool InThereshold
		{
			get{
				return inThreshold;
			}
		}
		
		/// <summary>
		/// return if timer is active
		/// </summary>
		/// <returns></returns>
		public bool IsTimerActive
		{
			get
			{
				return isTimerActive;
			}
		}
		
		/// <summary>
		/// property to get standard deviation of force bool value
		/// </summary>
		/// <returns></returns>
		public bool InStdDevForveLimit
		{
			get
			{
				return inStdDevForveLimit;
			}
		}
		
		/// <summary>
		/// property to get standard deviation of force
		/// </summary>
		/// <returns></returns>
		public double StdDevForce
		{
			get
			{
				return stdDevForce;
			}
		}
		
		
		/// <summary>
		/// update the force value
		/// </summary>
		/// <returns></returns>
		public void UpdateForce(bool pressDown_tmp, bool pressUp_tmp)
		{
			// if the two key are pressed mantain actual force value
			if (pressDown_tmp && pressUp_tmp)
			{
				prevForce = actualForce;
				// veryfy if I'm in threshold
				VerifyThreshold();
			}
			// lese if I press only key to increase increase force until FORCE_MAX
			else if (pressUp_tmp)
			{
				prevForce = actualForce;
				actualForce++;
				if (actualForce > forceUpperLimit)
				{
					Debug.LogWarning("Force aout of threshold");
				}
				// give me an error if I go out of limit
				if (actualForce > FORCE_MAX)
				{
					Debug.LogError("Force aout of limit");
					actualForce = FORCE_MAX;
				}
				// veryfy if I'm in threshold
				VerifyThreshold();
			}
			// lese if I press only key to decrease force decrase until 0
			else if (pressDown_tmp)
			{
				prevForce = actualForce;
				actualForce--;
				if (actualForce < 0) actualForce = 0;
				// veryfy if I'm in threshold
				VerifyThreshold();
			}
			// else reset al force value
			else
			{
				ResetForce();
				Debug.LogWarning("No enough Force");
			}
		}
		
		/// <summary>
		/// Verify if I'm in force upper and lower limit
		/// </summary>
		/// <returns></returns>
		void VerifyThreshold()
		{
			if (actualForce >= forceLowerLimit && actualForce <= forceUpperLimit)
			{
				inThreshold = true;
				// compute the std deviation strating from 10 value to evaluate if I'm in cosntan value
				Nvalue++;
				meanValue += actualForce;
				value2 += actualForce * actualForce;
				if (Nvalue > 10)
				{
					stdDevForce = Math.Sqrt(value2 / Nvalue - meanValue / Nvalue * meanValue / Nvalue);
					// if i'm not in constant value reset std deviation parameter and the timer
					if (stdDevForce > stdDevForcelimit )
					{
						meanValue = actualForce;
						Nvalue = 1;
						stdDevForce = 0;
						value2 = actualForce * actualForce;
						inStdDevForveLimit = false;
						ResetTimer();
					}
					// else change bool value to true
					else inStdDevForveLimit = true;
				}
				Debug.LogWarning("inthreshold" + actualForce + " " + inThreshold + " " + meanValue * meanValue  + " " + Nvalue + " " + value2/Nvalue);
			}
			// else reset timer and said me that i'm not in threshold
			else
			{
				inThreshold = false;
				Debug.LogWarning("inthreshold" + inThreshold);
				ResetTimer();
			}
		}
		
		/// <summary>
		/// Reset timer
		/// </summary>
		/// <returns></returns>
		void ResetTimer()
		{
			remainingTime = 0;
			isTimerActive = false;
			meanValue = 0;
			Nvalue = 0;
			stdDevForce = 0;
			value2 = 0;
			inStdDevForveLimit = false;
		}
		
		/// <summary>
		/// Reset force value
		/// </summary>
		/// <returns></returns>
		public void ResetForce()
		{
			actualForce = 0;
			prevForce = actualForce;
			meanValue = 0;
			Nvalue = 0;
			stdDevForce = 0;
			value2 = 0;
			inStdDevForveLimit = false;
			ResetTimer();
			inThreshold = false;
		}
		
		/// <summary>
		/// Start timer
		/// </summary>
		/// <returns></returns>
		public void StartTimer()
		{
			remainingTime = remainingTimeSetted;
			isTimerActive = true;
		}
		
		/// <summary>
		/// update timer
		/// </summary>
		/// <returns></returns>
		public bool updateTimer()
		{
			remainingTime -= Time.deltaTime;
			if (remainingTime <= 0)
			{
				ResetTimer();
				return true;
			}
			else return false;
		}
	}
 }

