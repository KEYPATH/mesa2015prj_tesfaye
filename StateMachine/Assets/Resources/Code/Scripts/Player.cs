﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Resources.Code.States;

public class Player : MonoBehaviour
{
	public GameObject explosion;

	private float restartLevelDelay = 1f;        //Delay time in seconds to restart level.
	private int pointsPerFood = 10;              //Number of points to add to player food points when picking up a food object.
	public int pointsPerKey = 20;              //Number of points to add to player food points when picking up a soda object.
	public int lossPerEnemy = 10;
	private float maxSpeed = 10f;
	public int playerFoodPoints = 100;
	private float speed; 
	private Rigidbody2D rb;
	private float min_distance = 0.5f;
	private float distanceOfCollider ; 
	private float distance ;
	Vector3 savePose;
	private BoxCollider2D box;
	public string sortingName = "ExplosionLayer";


	  
	Vector3
		screenPoint,offset,scanPos,	curPosition,curScreenPoint,
		mousePosWorld;

	Vector2 mousePos;
	RaycastHit hit;
	bool checkHit = false; 
	Vector3 currentPos;
	private bool _mouseState;
	private bool disableDrag = false;
	private bool disableMovement = false; 
	private Vector3 screenSpace;
	private int foodPoint; 
	private int food = 0;
	private bool isPlayerDie = false; 
    float xPos, yPos, length, height, centerX, centerY;
	float leftX, rightX, bottomY, uppperY;
	//Start overrides the Start function of MovingObject
	void Start () 
	{
		rb = GetComponent<Rigidbody2D> ();
	
		box = GetComponent<BoxCollider2D> ();

		GameObject.FindWithTag ("Player").transform.Rotate (new Vector3 (0, 0, 0));
      
	}

	//This function is called when the behaviour becomes disabled or inactive.
	private void OnDisable ()
	{
		//When Player object is disabled, store the current local food total in the GameManager so it can be re-loaded in next level.
		playerFoodPoints = food;
	}

	void Update ()
	{

		if (rb.velocity.magnitude > maxSpeed) {
			rb.velocity = Vector3.ClampMagnitude (rb.velocity, maxSpeed);
		}
		currentPos = gameObject.transform.position;
		
		savePose = currentPos;
		xPos = box.offset.x;
		yPos = box.offset.y;
		length = box.size.x;
		height = box.size.y;
		centerX = box.bounds.center.x;
		centerY = box.bounds.center.y;



		leftX = centerX - length / 2;
		rightX = centerX + length / 2;
		bottomY = centerY - height / 2;
		uppperY = centerY + height / 2;

		if (Input.GetMouseButtonDown (0)) {

		

			mousePosWorld = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			mousePos.x = mousePosWorld.x;
			mousePos.y = mousePosWorld.y;
			this.transform.position = Vector3.MoveTowards (transform.position, mousePosWorld, speed * Time.deltaTime);
			//if (collider == Physics2D.OverlapPoint(mousePos))
			if (mousePos.x >= leftX && mousePos.x <= rightX && mousePos.y <= uppperY && mousePos.y >= bottomY && disableDrag == true ) {
				disableDrag = false;

					   }
		             }
	          }
	
	void OnMouseDown ()
	{
		//if (disableDrag == false) {
			
		scanPos = gameObject.transform.position;
		screenPoint = Camera.main.WorldToScreenPoint (scanPos);
		offset = scanPos - Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
				
	
		//}
			
	}
			
	void OnMouseDrag ()
	{
		if (disableDrag == false) {
			distanceOfCollider = min_distance;
		
			curScreenPoint = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
			curPosition = Camera.main.ScreenToWorldPoint (curScreenPoint) + offset;
				
			
			transform.position = curPosition;

		}


	}
			

	
	//OnTriggerEnter2D is sent when another object enters a trigger collider attached to this object (2D physics only).
	void OnTriggerEnter2D (Collider2D other)
	{
		//Check if the tag of the trigger collided with is Exit.
		if (other.tag == "Exit") {
			//Invoke the Restart function to start the next level with a delay of restartLevelDelay (default 1 second).
			Invoke ("Restart", restartLevelDelay);
			
			//Disable the player object since level is over.
			enabled = false;
		}
		
		//Check if the tag of the trigger collided with is Food.
		else if (other.tag == "Food") {
			//Add pointsPerFood to the players current food total.
			food += pointsPerFood;

			other.gameObject.SetActive (false);

		} else if (other.tag == "Key") {
		
			food += pointsPerKey;
			other.gameObject.SetActive (false);

		}


	}

	//Restart reloads the scene when called.


	void OnCollisionEnter2D (Collision2D coll)
	{
	
		if (coll.gameObject.tag == "Enemy") {
			transform.position = currentPos;
			//Destroy(GameObject.FindWithTag("Player"));
			LoseFood (lossPerEnemy);
		
			//	Invoke ("Restart", restartLevelDelay);


			GameObject gogo = Instantiate(explosion, transform.position, Quaternion.identity) as GameObject;
	     

			StartCoroutine(WaitForExplosionToEnd(0.5f));

			Destroy(gogo,0.5f);
		//	GameObject play = Instantiate(playerGuy, new Vector3(currentPos.x, currentPos.y, currentPos.z) , Quaternion.identity) as GameObject;

		}

		if (coll.gameObject.tag == "Arrow") {

			if (GameObject.FindWithTag ("Arrow").transform.rotation.z == 90.0f) {

				if (gameObject.transform.position.x > GameObject.FindWithTag ("Arrow").GetComponent<BoxCollider2D> ().bounds.center.x) {
					GameObject.FindWithTag  ("Arrow").GetComponent<BoxCollider2D> ().isTrigger = true;
			
					disableMovement = true;
		     	
				} else {
					GameObject.FindWithTag ("Arrow").GetComponent<BoxCollider2D> ().isTrigger = false;

				}
			}

		}

		if (coll.gameObject.tag == "Walls") {

		
			disableDrag = true;

//			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
//			
//			checkHit = Physics.Raycast (ray, out hit);
//			


			distance = (currentPos - coll.gameObject.transform.position).magnitude;
			distanceOfCollider = FindClosestWall ();
		
//			if (distanceOfCollider < min_distance) {
//			
//			
//				transform.position = savePose;
//
//			}
		}

	}
	IEnumerator WaitForExplosionToEnd(float explosionTime)
	{
		yield return new WaitForSeconds (explosionTime);
       

	}

	float FindClosestWall ()
	{
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag ("Enemy");
		GameObject closest = null;
		float distance = Mathf.Infinity;
		Vector2 position = transform.position;
		foreach (GameObject go in gos) {
			Vector2 poseOne = go.transform.position;
			Vector2 diff = poseOne - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance) {
				closest = go;
				distance = curDistance;
			}
		}
		//return closest;
		return distance;
	
	}
	
	public  void Restart ()
	{
		//Load the last scene loaded, in this case Main, the only scene in the game.
		Application.LoadLevel (Application.loadedLevel);
	}
	
	
	//LoseFood is called when an enemy attacks the player.
	//It takes a parameter loss which specifies how many points to lose.
	public void LoseFood (int loss)
	{
		//Set the trigger for the player animator to transition to the playerHit animation.
		//animator.SetTrigger ("playerHit");

		food -= loss;
		
		//Check to see if game has ended.

	}
	
	
	//CheckIfGameOver checks if the player is out of food points and if so, ends the game.
    

	
	public string GetPoints ()
	{
		return food.ToString () + "/" + foodPoint.ToString ();
			
	}
}
