﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class BoardManagerNew : MonoBehaviour {

	public List <string> levels = new List<string>();
	
	public enum State {Menu, InGame, Options, LevelSelect}
	
	public State state = State.Menu;
	private Player getPoints;
	
	public GameObject mapManager;

	float timer = 10.0f;
	bool show = false;
	int time;
	
	IEnumerator  WaitForBoardToLoad(string levelName)
		
	{
		GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
		myButtonStyle.fontSize = 60;
		
		myButtonStyle.normal.textColor = Color.red;
		myButtonStyle.hover.textColor = Color.red;
		yield return new WaitForSeconds (.5f);
		BoardManage.GetBoardManager ().CreateLayoutMap (levelName);
		
		
		
	}
	IEnumerator ShowMessage(float delay)
	{
		yield return new WaitForSeconds (delay);
		show = false;
		
	}
	
	IEnumerator DelayBeforeLoad(float min){
		
		yield return new WaitForSeconds(min);
	}


	void LevelSelect()
	{

		GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
		myButtonStyle.fontSize = 60;
		
		myButtonStyle.normal.textColor = Color.red;
		myButtonStyle.hover.textColor = Color.red;

		for (int i = 0; i<levels.Count; i++) {
			
			if (GUI.Button (new Rect (Screen.width / 2 - 200, Screen.height/4 + 200* i-100, 400, 100), levels[i],myButtonStyle)) {
				
				timer -= Time.deltaTime;
				time = (int)timer;
				
				if (time > 0) {
					show = true;
					DelayBeforeLoad (2.0f);
					
					GUI.Box (new Rect (Screen.width / 2, Screen.height / 2, 200, 100), "" + time.ToString (),myButtonStyle);
				}

			 	Ingame();

				Instantiate (mapManager);
				
				StartCoroutine(WaitForBoardToLoad(levels[i]));
				
			}
			
		}
	}

	void Ingame()
	{
		
		
		//GUI.Label(new Rect(Screen.width/2-30 ,0,140, 50),"Score",getPoints.GetPoints());
		if (GUI.Button(new Rect (30,30, 100,30),"Quit"))
		{

			//Destroy(GameDataRef.GetBoardManager().gameObject);

			//state = State.Menu;
			
		}

   }

}