﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Resources.Code.Scripts;

namespace Assets.Resources.Code.Scripts
{
	public class AStar : MonoBehaviour
	{


		Vector3 startPos, targetPos, playerPosition;
		GameData  gamedata;
 
		void Awake ()
		{
			gamedata = GetComponent<GameData> ();
		}

		void update ()
		{
			playerPosition = GameObject.FindWithTag ("Player").transform.localPosition;
			targetPos = gamedata.targetPosition;
			startPos = gamedata.startPosition;
			PathFinding (startPos, targetPos); 
		}

		void PathFinding (Vector3 startPos, Vector3 targetPos)
		{

			// starting position = position of player
			// tartget position = position of exit

			Slot startSlot = gamedata.slotPlayer;
			Slot targetSlot = gamedata.slotExit;
			startSlot.transform.localPosition = startPos;
			targetSlot.transform.localPosition = targetPos;

			List<Slot> openSet = new List<Slot> ();
			HashSet <Slot> closedSet = new HashSet<Slot> ();
			openSet.Add (startSlot);
			while (openSet.Count>0) {

				Slot currentSlot = openSet [0];

				for (int i =1; i < openSet.Count; i++) {
					// if current Slot fcost is lower we set that to currrent Slot if not we compare the host to target position 
					if (openSet [i].fCost < currentSlot.fCost || openSet [i].fCost == currentSlot.fCost && openSet [i].hCost < currentSlot.fCost) {

						currentSlot = openSet [i];

					}

				}
				// if current Slot is current opeset[] Slot we reset current Slot  to openset
				//<summary>
				// remove from opeset
				// add to closed set
				//<summary>

				openSet.Remove (currentSlot);
				closedSet.Add (currentSlot);

				if (currentSlot == targetSlot) {
					RetracePath (startSlot, targetSlot);
					return;
				}
				foreach (Slot neighbour in gamedata.GetNeighbourhoodSlot(currentSlot)) {

					if (!neighbour.walkable || closedSet.Contains (neighbour)) {

						continue;
					}
					int newMovementCostToNeighbour = currentSlot.gCost + GetDistance (currentSlot, neighbour);

					if (newMovementCostToNeighbour < neighbour.gCost || openSet.Contains (neighbour)) {

						neighbour.gCost = newMovementCostToNeighbour;
						neighbour.hCost = GetDistance (neighbour, targetSlot);
						neighbour.parent = currentSlot;
					
						if (!openSet.Contains (neighbour))
							openSet.Add (neighbour);
					}
				}
			}
		}

		void RetracePath (Slot startSlot, Slot endSlot)
		{
			List<Slot> path = new List<Slot> ();
			Slot currentSlot = endSlot;

			while (currentSlot != startSlot) {
		
				path.Add (currentSlot);
				currentSlot = currentSlot.parent;
			

			}
			path.Reverse ();

			gamedata.path = path;
		}

		int GetDistance (Slot SlotA, Slot SlotB)
		{
			int distX = Mathf.Abs (SlotA.gridX - SlotB.gridY);
			int distY = Mathf.Abs (SlotA.gridX - SlotB.gridY);


			if (distX > distY) {
				return  14 * distY + 10 * (distX - distY);

			} else {
				return  14 * distX + 10 * (distY - distX);
			}
		}
	}
}