﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UserInterface : MonoBehaviour {


	public List <string> levels = new List<string>();
	public enum State {Menu, InGame, Options, LevelSelect}

	public State state = State.Menu;
	private Player getPoints;
	int index = 0;
	int indexQuit = 0;
	string message = "Go";

	
	public GameObject mapManager;
	public GameObject backgroundPrefab;
	public GameObject copyBackGround;
	GameObject background;
	GameObject backgroundCopy;

	float timer = 10.0f;
	bool show = false;
	int time;

	IEnumerator  WaitForBoardToLoad(string levelName)

	{
		GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
		myButtonStyle.fontSize = 60;
		
		myButtonStyle.normal.textColor = Color.red;
		myButtonStyle.hover.textColor = Color.red;
		yield return new WaitForSeconds (.5f);
		BoardManage.GetBoardManager ().CreateLayoutMap (levelName);

	

	}
	IEnumerator ShowMessage(float delay)
	{
		yield return new WaitForSeconds (delay);
		show = false;
		
	}
	
	IEnumerator DelayBeforeLoad(float min){
		
		yield return new WaitForSeconds(min);
	}



	void OnGUI()
	{
		Background ();


		index++;
		GUI.backgroundColor = Color.grey;
		switch (state) {
		
		case State.Menu:

			Menu ();
			break;
		case State.InGame:
			Ingame ();

			break;
		case State.LevelSelect:
			LevelSelect();

			break;

		case State.Options:
			Options ();
			break;
		
		}
	}

		void Menu()
		{

		GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
		myButtonStyle.fontSize = 60;
		
		myButtonStyle.normal.textColor = Color.red;
		myButtonStyle.hover.textColor = Color.red;
		GUI.Label(new Rect(Screen.width/2-300 ,75,600, 150),"Cheese Hunt Game",myButtonStyle);

			if (GUI.Button(new Rect(Screen.width/2-300, Screen.height/2,600,100),"Play Game", myButtonStyle))
			{


				state =State.LevelSelect;
				//Instantiate(mapManager);
				
			}
			if (GUI.Button(new Rect(Screen.width/2-200, Screen.height/2 +105,400,100),"Options",myButtonStyle))
			{
				state =State.Options;
				
			}
		}
   void LevelSelect()
	  {

		 if (indexQuit == 1) {
			indexQuit =0;
			backgroundCopy = Instantiate (copyBackGround, new Vector3 (0f, 0f, 0f), Quaternion.identity)as GameObject;
		}
		GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
		myButtonStyle.fontSize = 60;
		
		myButtonStyle.normal.textColor = Color.red;
		myButtonStyle.hover.textColor = Color.red;
//		if (GUI.Button (new Rect (Screen.width / 2 - 200, Screen.height/5-100, 400, 100), "Level One",myButtonStyle)) {
//			if (backgroundCopy != null){
//			Destroy(backgroundCopy);
//			}
//			state = State.InGame;
//			Destroy(background);
//			Instantiate (mapManager);
//			StartCoroutine(WaitForBoardToLoad("Easy"));
//		}
		for (int i = 0; i<levels.Count; i++) {
		
			if (GUI.Button (new Rect (Screen.width / 2 - 200, Screen.height/4 + 200* i-100, 400, 100), levels[i],myButtonStyle)) {

				timer -= Time.deltaTime;
				time = (int)timer;
				
				if (time > 0) {
					show = true;
					DelayBeforeLoad (2.0f);
					
					GUI.Box (new Rect (Screen.width / 2, Screen.height / 2, 200, 100), "" + time.ToString (),myButtonStyle);
				}
				
				else if(show) {
					
					StartCoroutine (ShowMessage (2.0f));
					GUI.Box(new Rect(Screen.width / 2, Screen.height / 2 ,200 ,100), message, myButtonStyle);
				}


				if (backgroundCopy != null)
				{
				Destroy(backgroundCopy);
				}
			
				state = State.InGame;

				Destroy(background);


				Instantiate (mapManager);

				StartCoroutine(WaitForBoardToLoad(levels[i]));
			
			}

		}
	}

	void Ingame()
		{

				
	 //GUI.Label(new Rect(Screen.width/2-30 ,0,140, 50),"Score",getPoints.GetPoints());
		if (GUI.Button(new Rect (30,30, 100,30),"Quit"))
		{
			Destroy(background);
			index = 0;
			indexQuit +=1;

			Destroy(BoardManage.GetBoardManager().gameObject);
//			if (index == 0) {
//				
//				background = Instantiate (backgroundPrefab, new Vector3 (0f, 0f, 0f), Quaternion.identity)as GameObject;
//				//Instantiate (background);
//			}
			state = State.Menu;

		}

	

		}

		void Options()
		{
		GUI.Label(new Rect(Screen.width/2-30,0,140, 50),"Options");
		if (GUI.Button(new Rect(Screen.width/2-50, Screen.height/2 ,100,30),"Back"))
		{
			//Destroy(BoardManage.GetBoardManager().gameObject);
			state = State.Menu;

		}
		Color c = StateManager2.Instance ().difficulty == StateManager2.GameDifficulty.Easy ? Color.red : Color.green;
		GUI.color = c;
		if (GUI.Button(new Rect(Screen.width/2-50, Screen.height/2 +35,100,30),"Easy"))
		{
			//Destroy(BoardManage.GetBoardManager().gameObject);
			//Instantiate(easyMap);

			StateManager2.Instance().SetDifficulty(StateManager2.GameDifficulty.Easy);

			
		}
		c = StateManager2.Instance ().difficulty == StateManager2.GameDifficulty.Easy ? Color.red : Color.green;
		GUI.color = c;
		if (GUI.Button(new Rect(Screen.width/2-50, Screen.height/2 +70,100,30),"Medium"))
		{
			//Instantiate(mediumMap);
			//Destroy(BoardManage.GetBoardManager().gameObject);

			StateManager2.Instance().SetDifficulty(StateManager2.GameDifficulty.Medium);
		}
		c = StateManager2.Instance ().difficulty == StateManager2.GameDifficulty.Easy ? Color.red : Color.green;
		GUI.color = c;
		if (GUI.Button(new Rect(Screen.width/2-50, Screen.height/2 + 105,100,30),"Hard"))
		{
			//Instantiate(hardMap);
		//	Destroy(BoardManage.GetBoardManager().gameObject);
			StateManager2.Instance().SetDifficulty(StateManager2.GameDifficulty.Hard);
				
		     }
		  }
	void Background()
	{
		if (index == 0) {
			
			background = Instantiate (backgroundPrefab, new Vector3 (0f, 0f, 0f), Quaternion.identity)as GameObject;
	
		}
		
	}
	}

