using UnityEngine;
using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class StateManager2  {
	private static StateManager2 instance;              //Static instance of GameManager which allows it to be accessed by any other script.
	private  StateManager2(){}

	public static StateManager2 Instance()
	{
	if (instance == null) {
			instance = new StateManager2 ();
		
		}
		return instance;
	}




	public enum GameDifficulty{Easy,Medium, Hard}
	public GameDifficulty difficulty = GameDifficulty.Easy;

	private string[] easyDifficulty = {"Easy"};
	private string[] mediumDifficulty ={ "Medium"};
	private string[] hardDifficulty = {"Hard"};


	public List<string> LevelOfGame

	{

		get {

		 List<string> tempLevel = new List<string>();
			switch(difficulty)
			{

			case GameDifficulty.Easy:

				tempLevel.AddRange(easyDifficulty);

				break;
			case GameDifficulty.Medium:
				tempLevel.AddRange(mediumDifficulty);
				break;
			case GameDifficulty.Hard:
				tempLevel.AddRange(hardDifficulty);
				break;

			}
			return tempLevel;


		}



	}
	public void SetDifficulty(GameDifficulty diff)
	{
		difficulty = diff;
	}

	public string ReturnLevel(){  // get random type

		return LevelOfGame[0];

	}

//	void Awake()
//	{
//		//Check if instance already exists
//		if (instance == null)
//			
//			//if not, set instance to this
//			instance = this;
//		
//		//If instance already exists and it's not this:
//		else if (instance != this)
//			
//			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
//			Destroy(gameObject);    
//		
//		//Sets this to not be destroyed when reloading scene
//		DontDestroyOnLoad(gameObject);
//		
//		//Get a component reference to the attached BoardManager script
//		boardScript = GetComponent<BoardManager>();
//		
//	
//	}


//	void GameOver()
//	{
//		enabled = false;
//	}
//
//


}
